const express = require('express');
const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.static(`${__dirname}/views`));

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/views/index.html`);
});

app.get('/sobre', (req, res) => {
  res.sendFile(`${__dirname}/views/sobre.html`);
});

app.get('/contato', (req, res) => {
  res.sendFile(`${__dirname}/views/contato.html`);
});

app.get('/confirmacao', (req, res) => {
  const { email, nome, mensagem } = req.query;
  const msg = `Obrigado ${nome} por ter enviado a mensagem: ${mensagem}. Retornaremos no e-mail: ${email}`;
  res.json({ msg });
});

app.listen(3000, () => console.log('Server running on port 3000'))